Source: connid
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org> 
Uploaders: Christopher Hoskin <mans0954@debian.org>
Build-Depends: debhelper (>= 10), maven-debian-helper,
 libmaven-bundle-plugin-java,
 testng,
 libfest-assert-java,
 groovy,
 libmaven-dependency-plugin-java,
 libmaven-verifier-java,
 liblogback-java
Build-Conflicts: libconnid-java
Standards-Version: 4.0.0
Section: java
Homepage: http://connid.tirasa.net/
Vcs-Git: https://anonscm.debian.org/git/pkg-java/connid.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/connid.git

Package: libconnid-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}
Description: framework for provisioning identities to repositories
 Connectors for Identity Management (ConnId) is a framework for developing
 identity connectors, the technology layer that takes place in the exchange
 of identity-related information (password, attributes) between identity
 managers (such as Apache Syncope) and identity repositories (e.g. LDAP
 directories, relational databases).

Package: connid-server
Architecture: all
Depends: ${misc:Depends}, libconnid-java, adduser
Description: ConnId Java Connection Server
 A connector server is required when a connector bundle is not directly executed
 within your application. By using one or more connector servers, the connector
 architecture thus permits your application to communicate with externally
 deployed bundles.
 .
 A Java connector server is useful when you do not wish to execute a Java
 connector bundle in the same VM as your application. It may be beneficial to
 run a Java connector on a different host for performance improvements if the
 bundle works faster when deployed on the same host as the native managed
 resource. Additionally, one may wish to use a Java connector server under a
 Java remote connector server in order to eliminate the possibility of an
 application VM crash due to a fault in a JNI-based connector.
